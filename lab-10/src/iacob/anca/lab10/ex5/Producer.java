package iacob.anca.lab10.ex5;

class Producer implements Runnable
{
    private Buffer bf;
    private Thread thread;
    Producer(Buffer bf){this.bf=bf;}

    public void start()
    {
        if (thread==null)
        {
            thread = new Thread(this);
            thread.start();
        }
    }

    public void run()
    {
        while (true)
        {
            bf.push(Math.random());
            System.out.println("Am scris.");
            try
            {Thread.sleep(1000);}catch(Exception e){}
        }
    }
}

/**
 * Aceasta este o clasa de tip fir de executie. Intr-o bucla infinita sunt citite elemente
 * din cadrul unui obiect de tip Buffer.
 */

class Consumer extends Thread
{
    private Buffer bf;
    Consumer(Buffer bf){this.bf=bf;}

    public void run()
    {
        while (true)
        {
            System.out.println("Am citit : "+this+" >> "+bf.get());
        }
    }
}