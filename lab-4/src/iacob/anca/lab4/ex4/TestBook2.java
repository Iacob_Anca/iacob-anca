package iacob.anca.lab4.ex4;
import java.util.Arrays;
import iacob.anca.lab4.ex2.Author;


public class TestBook2 {
    public static void main(String[] args) {
        Author[] aut = new Author[3];
        aut[0] = new Author("Agatha Christi", "agatha@mail.com", 'm');
        aut[1] = new Author("Mihai Eminescu", "meminescu@yahoo.com", 'm');
        aut[2] = new Author("Liviu Rebreanu", "mobama@gmail.com", 'm');
        Book b = new Book("El", aut, 50, 20);
        Book b1 = new Book("Poezii", aut, 20, 15);
        Book b2 = new Book("Ion", aut, 30, 10);
        System.out.println(b.toString() + "\n");
        b.setPrice(20.5);
        b.setQtyInStock(40);
        b.printAuthors();
        b1.setPrice(30);
        b1.setQtyInStock(15);
        b1.printAuthors();
        b2.setPrice(50.1);
        b2.setQtyInStock(20);
        b2.printAuthors();
        System.out.println(Arrays.toString(b.getAuthor()) + " " + b.getName() + " " + b.getPrice() + " " + b.getQtyInStock());
        System.out.println(Arrays.toString(b1.getAuthor()) + " " + b1.getName() + " " + b1.getPrice() + " " + b1.getQtyInStock());
        System.out.println(Arrays.toString(b2.getAuthor()) + " " + b2.getName() + " " + b2.getPrice() + " " + b2.getQtyInStock());
    }
}
