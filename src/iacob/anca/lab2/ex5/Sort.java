package iacob.anca.lab2.ex5;
import java.io.*;
public class Sort {

    public static void main(String[] args) throws IOException {
        int[] v = new int[10];
        int i;
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        for (i = 0; i < 10; i++)
            v[i] = Integer.parseInt(stdin.readLine());
        int aux = 0;
        boolean stop = true;
        do {
            stop = true;
            for (i = 0; i < 9; i++)
                if (v[i] > v[i + 1])
                {
                    aux = v[i + 1];
                    v[i + 1] = v[i];
                    v[i] = aux;
                    stop = false;
                }
        }
        while (stop == false);
        for (i = 0; i < 10; i++)
            System.out.print(v[i] + " ");
    }

}
