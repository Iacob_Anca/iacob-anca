package iacob.anca.lab8.ex1;

public class Test {
    public static void main(String[] args){
        Persoana p1 = new Persoana("Ion", 40);
        ContBancar c = new ContBancar(p1, 0.1);

        c.detalii();
        c.depunere(300);
        c.actualizare();
        c.detalii();

        ManagerConturi mc = new ManagerConturi();

        Persoana a = new Persoana("Anca",21);
        Persoana b = new Persoana("Ana",20);
        Persoana e = new Persoana("Maria",30);

        mc.creareCont(a, 0.03);
        mc.creareCont(b, 0.05);
        mc.creareCont(e, 0.8);

        mc.afiseaza();
    }
}
