package iacob.anca.lab5.ex2;

public interface Image {
    void display();
    void RotatedImage();
}

class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Displaying " + fileName);
    }

    @Override
    public void RotatedImage(){
        System.out.println("Display rotated"+fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}

class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;
    private int arg = 0;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    public ProxyImage(String fileName, int arg){
        this.fileName = fileName;
        this.arg = arg;
    }

    @Override
    public void display() {
        if(arg == 0) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
        else RotatedImage();
    }

    @Override
    public void RotatedImage() {
        if (arg == 1) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.RotatedImage();
        }
        else display();
    }
}