package iacob.anca.lab7.ex1;

public class CoffeeDrinker {
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException, OutOfNumberException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Coffee concentration to high!");
        if(Coffee.numberCoffee>10)
            throw new OutOfNumberException(Coffee.getNumberCoffee(),"Out of coffee!");
        System.out.println("Drink coffee:"+c);
    }
}
