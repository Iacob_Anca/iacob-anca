package iacob.anca.lab4.ex3;
import iacob.anca.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author A = new Author("Iacob", "ancaiacob_98@yahoo.com", 'f');
        Book B1 = new Book("Iacob","ancaiacob_98@yahoo.com",'f',"Fire and Blood",A,150);
        Book B2 = new Book("Ardelean","ardelean.ana@gmail",'f',"Sky in the deep",A,100,4);
        System.out.println("The author of B1 is "+ B1.getAuthor());
        System.out.println("The price of the book 'The Book' is "+ B1.getPrice());
        B1.setPrice(200);
        System.out.println("The new price of the book 'Fire and Blood' is "+ B1.getPrice());
        System.out.println("The quantity of the book 'Sky in the deep' is "+ B2.getQtyInStock());
    }
}
