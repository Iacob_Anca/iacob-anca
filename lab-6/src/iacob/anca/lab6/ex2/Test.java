package iacob.anca.lab6.ex2;

public class Test {
    public static void main(String[] args) {
        Bank b = new Bank();
        BankAccount b1 = new BankAccount("owner1", 20);
        b1.withdraw(30);
        b1.deposit(50);
        b1.withdraw(40);

        b.addAccount("abcd", 50);
        b.addAccount("cdef", 300);
        b.addAccount("aaaa", 30);

        b.printAccounts(100, 400);
        b.printAccounts();
        b.getAllAccounts();

    }
    }
