package iacob.anca.lab7.ex1;

public class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){
            Coffee c = mk.makeCoffee();
            try {
                d.drinkCoffee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            } catch (OutOfNumberException e) {
                System.out.println("Exception:"+e.getMessage()+" number of coffee="+e.getNumber());
            }
            finally{
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}
